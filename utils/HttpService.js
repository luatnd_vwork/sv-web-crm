export const STATUS_CODE = {
  SUCCESS: 200,
};

/**
 * Generate query string from JSON object, with safe encodeURIComponent
 * @param {{}} queryStringData
 * @return string the
 * @deprecated Please use require('query-string').parse()  / .stringify() instead
 */
export const encoder = queryStringData => Object.keys(queryStringData)
  .map(key => `${encodeURIComponent(key)}=${encodeURIComponent(queryStringData[key])}`)
  .join('&');

/**
 * Fake an async request return $data after $time

 Usage:
 console.log("Start calling fake API");
 const requestTimeout = 2500;
 const responseDataFake = {
      status: Math.random() > 0.4 ? 200 : 400, // Random success or failure
      data: bookingFormSuccessResponse,
    };
 const response = yield call(fakeApiRequest(responseDataFake, requestTimeout));
 response.data.data = {...action.payload};
 console.log('Finish fake API: response: ', {...response});

 * @param fakeResponse
 * @param time
 */
export const fakeApiRequestGenerator = (fakeResponse, time) => () => {
  return new Promise(resolve => {
    setTimeout(() => {
      resolve(fakeResponse)
    }, time);
  })
};

/**
 * const response = await fakeApiRequestAsyncAwait(fakeResponse, time);
 */
export function fakeApiRequestAsyncAwait(fakeResponse, time) {
  return new Promise((resolve) => {
    setTimeout(() => resolve(fakeResponse), time)
  })
}

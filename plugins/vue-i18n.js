import Vue from 'vue'
import VueI18n from 'vue-i18n'

export default () => {
  Vue.use(VueI18n)
}

import Vue from 'vue'
import Element from 'element-ui'
import locale from 'element-ui/lib/locale/lang/en'

/**
 * Custom theme
 */
import './element-ui-variables.scss'

export default () => {
  Vue.use(Element, {
    locale,
    // size: "small"
  })
}

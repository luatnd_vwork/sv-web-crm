# The development best practice

## Container & presentational component 

This is apply for Reactjs and Vue.
Plz google to learn more.
https://medium.com/@dan_abramov/smart-and-dumb-components-7ca2f9a7c7d0

## VueX
For Vuex action pattern and best practice, the pros and cons of each practice, plz see the guide:
 https://vuejsdevelopers.com/2017/08/28/vue-js-ajax-recipes/


**Sync data between FormData and URL**<br/>
See: components/....../EmployerListTab/Form/index.vue:251<br/>
And: components/....../EmployerListTab/DataTable/store.js:98<br/>

**Debug Vuex:**

Simple way:
```js
$nuxt.$store.state.Employer.EmployerListTab
```

More complicated way:
Run this js code in chrome dev tool to see all your Vuex store
```js
RootStore()._mutations
RootStore()._modulesNamespaceMap
RootStore()._modules.root._children

function getModule(store, path) {
	const root = store._modules.root;	
	let data = root;

	const mods	= path.split('/');
	for(let i=0; i< mods.length; i++)	{
		data = data._children[mods[i]];
	}

	return data;
}
function getStateValueOf(store, path) {
	const root = store._modules.root;	
	let data = root;

	const mods	= path.split('/');
	for(let i=0; i< mods.length-1; i++)	{
		data = data._children[mods[i]];
	}

	data = data.state[mods.pop()];

	return data;
}


m = getModule(RootStore(), 'Employer/EmployerListTab/Form')
s = getStateValueOf(RootStore(), 'Employer/EmployerListTab/Form/formData')
a.formInput1;
a.formInput1 = 'my test update data';
```


## Common Error
Vuex update both namespaced module ?
Problem: When commit an action --> Both module was updated!

Why: 
Because you accidentally set two EmployerListTab.Form and EmployerListVipTab.Form to the same formData, it's mean same object, same pointer.
That's why!

How to avoid and practice:
* Always clone object when you mutate an store. This will shallow copy. But it still have issue when you use nested object.
* If you update a nested object to the store, alway care about your pointer if your state was mutate by multiple place or by dynamic mutation. So consider lodash._cloneDeep() 


## SSR do not work with node_modules contain *.vue file

Some npm component does not work in SSR mode because its is Vuejs component.

JSX is sugar js so that JSX work well with SSR. 

~~But Vue SFC contain not only JS but html template and CSS.~~
~~So they need to be parsed via vue-loader.~~

~~The problem is SSR run on nodejs, node consider all node_modules/ as node module (in node code).
So you need to find a way to make nodejs consider node_modules as vue file.~~

All above hypothesis is not true,
The error is
> Cannot find module 'vue-ionicons/dist/ios-expand' from '/MyWorkspace/source/sieuviet/sv-web-erp-demo'
===> The error is vueloader could not find the file the: 
>vue-ionicons/dist/ios-expand/index.js
But the correct path we need is:
>vue-ionicons/dist/ios-expand.vue
That's why!


#### Method 1: Make webpack vue-loader recognize .vue file
By default, vue-loader recognize only file.js, not file.vue.

https://github.com/vuejs/vue-loader/issues/685



#### Method 2: Nuxtjs plugin with `ssr:false`
To ignore ssr for that module

#### Method 3: Simply specify .vue when import
```
import iosIcon from 'vue-ionicons/dist/ios-expand.vue'
```



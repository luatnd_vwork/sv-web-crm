/**
 Copy json from

 aToObj = (a, i) => ({
     id: i,
     url: a.attr('href').replace('http://crm01.sieuviet-dev.com:8080', ''),
     label: a.text().trim(),
     icon: 'el-icon-setting',
 });
 json = {};
 $('#left-panel > nav > ul').find(' > li').each((i, e) => {
    const a = $(e).find('a:first');
    json[i] = aToObj(a, i);
    json[i]["children"] = {};

    $(e).find('>ul > li').each((j, sub_e) => {
      const sa = $(sub_e).find('a:first');
      json[i]["children"][j] =  aToObj(sa, j);
    })
  });
 JSON.stringify(json);

 */

const items = ['el-icon-circle-check-outline', 'el-icon-goods', 'el-icon-time', 'el-icon-service', 'el-icon-view', 'el-icon-star-off', 'el-icon-rank', 'el-icon-search', 'el-icon-setting'];
const getRandIcon = () => items[ Math.floor(Math.random() * items.length) ];
const objectMenu = {
  "0": { "id": 0, "url": "/", "label": "Home", "icon": getRandIcon(), "children": {} },
  "1": {
    "id": 1,
    "url": "#",
    "label": "Chăm sóc khách hàng",
    "icon": getRandIcon(),
    "children": {
      "0": { "id": 0, "url": "/SalesEmployerInformation?__tab=EmployerListTab", "label": "Nhà tuyển dụng", "icon": getRandIcon(), },
      "1": { "id": 1, "url": "/SalesEmployerJobNewsList", "label": "Tin tuyển dụng", "icon": getRandIcon(), },
      "2": { "id": 2, "url": "/SalesEmployerTouchs/List", "label": "Bán hàng", "icon": getRandIcon(), },
      "3": { "id": 3, "url": "/phieu-dang-ky/list", "label": "Phiếu đăng ký", "icon": getRandIcon(), },
      "4": { "id": 4, "url": "/SalesServiceRequest", "label": "Yêu cầu hạ dịch vụ, bảo lưu", "icon": getRandIcon(), },
      "5": { "id": 5, "url": "/SalesMinisite", "label": "Minisite chờ duyệt", "icon": getRandIcon(), },
      "6": { "id": 6, "url": "/#^/[/]$#u", "label": "Booking", "icon": getRandIcon(), },
      "7": { "id": 7, "url": "/#^/[/]$#u", "label": "Quản lý hủy booking", "icon": getRandIcon(), },
      "8": { "id": 8, "url": "/#^/[/]$#u", "label": "Thống kê booking (CSKH)", "icon": getRandIcon(), },
      "9": { "id": 9, "url": "/#^/[/]$#u", "label": "Quản lý đặt chỗ", "icon": getRandIcon(), },
      "10": { "id": 10, "url": "/#^/[/]$#u", "label": "Quản lý cấm booking", "icon": getRandIcon(), }
    }
  },
  "2": {
    "id": 2,
    "url": "#",
    "label": "Quản lý CSKH",
    "icon": getRandIcon(),
    "children": {
      "0": { "id": 0, "url": "/SalesMasterTeam/index", "label": "Nhóm bán hàng", "icon": getRandIcon(), },
      "1": { "id": 1, "url": "/#^/[/]$#u", "label": "Chỉ tiêu kinh doanh", "icon": getRandIcon(), },
      "2": { "id": 2, "url": "/#^/[/]$#u", "label": "Biểu đồ hiệu suất bán hàng", "icon": getRandIcon(), },
      "3": { "id": 3, "url": "/#^/[/]$#u", "label": "Báo cáo hiệu suất bán hàng", "icon": getRandIcon(), },
      "4": { "id": 4, "url": "/SalesMonitoringXliteSummary/index", "label": "Thống kê cuộc gọi", "icon": getRandIcon(), },
      "5": { "id": 5, "url": "/#^/[/]$#u", "label": "Chỉ tiêu doanh thu", "icon": getRandIcon(), },
      "6": { "id": 6, "url": "/#^/[/]$#u", "label": "Bản đồ doanh thu", "icon": getRandIcon(), },
      "7": { "id": 7, "url": "/AccountantCallCenterRank", "label": "Xếp loại", "icon": getRandIcon(), }
    }
  },
  "3": {
    "id": 3,
    "url": "#",
    "label": "Thông tin bán hàng",
    "icon": getRandIcon(),
    "children": {
      "0": { "id": 0, "url": "/AccountantServicePriceList", "label": "Bảng giá", "icon": getRandIcon(), },
      "1": { "id": 1, "url": "/AccountantServicePromotionDiscount", "label": "Giảm giá tái ký", "icon": getRandIcon(), },
      "2": { "id": 2, "url": "/AccountantServicePromotionFreeItem", "label": "Tặng dịch vụ", "icon": getRandIcon(), },
      "3": { "id": 3, "url": "/#^/[/]$#u", "label": "Kịch bản bán hàng", "icon": getRandIcon(), }
    }
  },
  "4": {
    "id": 4,
    "url": "#",
    "label": "Quản lý ứng viên",
    "icon": getRandIcon(),
    "children": {
      "0": { "id": 0, "url": "/#^/[/]$#u", "label": "Danh sách ứng viên", "icon": getRandIcon(), },
      "1": { "id": 1, "url": "/#^/[/]$#u", "label": "Hồ sơ ứng viên", "icon": getRandIcon(), }
    }
  },
  "5": {
    "id": 5,
    "url": "#",
    "label": "Tổng đài",
    "icon": getRandIcon(),
    "children": {
      "0": { "id": 0, "url": "/SalesMonitoringXliteHistory", "label": "Lịch sử & nghe lại cuộc gọi", "icon": getRandIcon(), },
      "1": { "id": 1, "url": "/#^/[/]$#u", "label": "Đổi chăm sóc khách hàng", "icon": getRandIcon(), },
      "2": { "id": 2, "url": "/XliteSummary", "label": "Thống kê cuộc gọi", "icon": getRandIcon(), },
      "3": { "id": 3, "url": "/AdminCallCenterViolation", "label": "Thống kê vi phạm", "icon": getRandIcon(), }
    }
  },
  "6": {
    "id": 6,
    "url": "#",
    "label": "Quản lý mục tiêu",
    "icon": getRandIcon(),
    "children": {
      "0": { "id": 0, "url": "/AccountantCallCenterKPI/index", "label": "Mục tiêu ngày hôm sau", "icon": getRandIcon(), },
      "1": { "id": 1, "url": "/SalesRevenueTarget/index", "label": "Mục tiêu hằng tuần", "icon": getRandIcon(), },
      "2": { "id": 2, "url": "/SalesRevenueTargetMonth/index", "label": "Mục tiêu tháng", "icon": getRandIcon(), },
      "3": { "id": 3, "url": "/SalesRevenueAchievement/index", "label": "Doanh thu hằng ngày", "icon": getRandIcon(), },
      "4": { "id": 4, "url": "/SalesRevenueTrackingProgress", "label": "Theo dõi tiến độ", "icon": getRandIcon(), },
      "5": { "id": 5, "url": "/SalesRevenueWeeklyPlan/index", "label": "Kế hoạch trong tuần", "icon": getRandIcon(), },
      "6": { "id": 6, "url": "/SalesRevenueMonthlyPlan/index", "label": "Kế hoạch trong tháng", "icon": getRandIcon(), },
      "7": { "id": 7, "url": "/#^/[/]$#u", "label": "Doanh thu hằng ngày (kế toán)", "icon": getRandIcon(), },
      "8": { "id": 8, "url": "/#^/[/]$#u", "label": "Thứ hạng cá nhân tháng", "icon": getRandIcon(), },
      "9": { "id": 9, "url": "/#^/[/]$#u", "label": "Thứ hạng nhóm tháng", "icon": getRandIcon(), },
      "10": { "id": 10, "url": "/#^/[/]$#u", "label": "Thứ hạng kỳ xét", "icon": getRandIcon(), }
    }
  },
  "7": {
    "id": 7,
    "url": "#",
    "label": "Kế toán",
    "icon": getRandIcon(),
    "children": {
      "0": { "id": 0, "url": "/AccountantServiceApproveRegistration/list", "label": "Duyệt phiếu đăng ký", "icon": getRandIcon(), },
      "1": { "id": 1, "url": "/AccountantJobNewsList", "label": "Duyệt kích tin", "icon": getRandIcon(), },
      "2": { "id": 2, "url": "/AccountantJobNewsListHistory", "label": "Lịch sử duyệt kích tin", "icon": getRandIcon(), },
      "3": { "id": 3, "url": "/Accountantapproverequest", "label": "Duyệt yêu cầu", "icon": getRandIcon(), },
      "4": { "id": 4, "url": "/AccountantServiceApproveCashAdjust", "label": "Duyệt giảm giá ngoài chính sách", "icon": getRandIcon(), },
      "5": { "id": 5, "url": "/#^/[/]$#u", "label": "Duyệt đặt chổ", "icon": getRandIcon(), },
      "6": { "id": 6, "url": "/AccountantMasterCustomer", "label": "Khách hàng", "icon": getRandIcon(), },
      "7": { "id": 7, "url": "/#^/[/]$#u", "label": "Công nợ & Thanh toán", "icon": getRandIcon(), },
      "8": { "id": 8, "url": "/#^/[/]$#u", "label": "Quản lý nhân viên thu nợ", "icon": getRandIcon(), },
      "9": { "id": 9, "url": "/#^/[/]$#u", "label": "Lịch sử công nợ", "icon": getRandIcon(), },
      "10": { "id": 10, "url": "/#^/[/]$#u", "label": "Báo cáo doanh thu", "icon": getRandIcon(), },
      "11": { "id": 11, "url": "/#^/[/]$#u", "label": "Báo cáo công nợ", "icon": getRandIcon(), },
      "12": { "id": 12, "url": "/AccountantSieuVietBankAccount", "label": "Tài khoản ngân hàng", "icon": getRandIcon(), },
      "13": { "id": 13, "url": "/#^/[/]$#u", "label": "Thống kê booking (kế toán)", "icon": getRandIcon(), }
    }
  },
  "8": {
    "id": 8,
    "url": "#",
    "label": "Dữ liệu danh mục",
    "icon": getRandIcon(),
    "children": {
      "0": { "id": 0, "url": "/AdminMasterChannel", "label": "Kênh bán", "icon": getRandIcon(), },
      "1": { "id": 1, "url": "/#^/[/]$#u", "label": "Cổng thông tin", "icon": getRandIcon(), },
      "2": { "id": 2, "url": "/AdminMasterService", "label": "Gói dịch vụ", "icon": getRandIcon(), },
      "3": { "id": 3, "url": "/AdminMasterEffect", "label": "Hiệu ứng", "icon": getRandIcon(), },
      "4": { "id": 4, "url": "/AdminMasterTerritory", "label": "Vị trí địa lý", "icon": getRandIcon(), },
      "5": { "id": 5, "url": "/AdminMasterHoliday", "label": "Lịch nghỉ", "icon": getRandIcon(), },
      "6": { "id": 6, "url": "/AdminBookingConfigBoxLimit", "label": "Giới hạn tin tính phí", "icon": getRandIcon(), },
      "7": { "id": 7, "url": "/AdminCallCenterTimeFrame", "label": "Khung giờ", "icon": getRandIcon(), },
      "8": { "id": 8, "url": "/AdminCallCenterMasterKPI", "label": "Mục tiêu bán hàng", "icon": getRandIcon(), },
      "9": { "id": 9, "url": "/AdminCallCenterMasterScoring", "label": "Tiêu chí chấm điểm", "icon": getRandIcon(), },
      "10": { "id": 10, "url": "/AdminConfigParameter", "label": "Thông số", "icon": getRandIcon(), },
      "11": { "id": 11, "url": "/AdminRevenueAchievement", "label": "Mốc đánh giá doanh thu", "icon": getRandIcon(), }
    }
  },
  "9": {
    "id": 9,
    "url": "#",
    "label": "Hệ thống",
    "icon": getRandIcon(),
    "children": {
      "0": { "id": 0, "url": "/nguoi-su-dung", "label": "Người dùng", "icon": getRandIcon(), },
      "1": { "id": 1, "url": "/nguoi-su-dung/nhom", "label": "Nhóm người dùng", "icon": getRandIcon(), },
      "2": { "id": 2, "url": "/AdminSyncData", "label": "Lịch sử đồng bộ dữ liệu", "icon": getRandIcon(), },
      "3": { "id": 3, "url": "/AdminConfigAnnouncement", "label": "Quản lý thông báo", "icon": getRandIcon(), }
    }
  }
};

export default objectMenu;

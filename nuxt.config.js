module.exports = {

  /**
   * Page render mode. 'spa' for backend,
   * If you have experience you can use 'universal' mode to support server side rendering (SSR)
   */
  mode: 'spa',

  /*
  ** Headers of the page
  */
  head: {
    title: 'Siêu Việt CRM',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Sieuviet web backend(ERP) demo by Vuejs' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/asset/default/img/favicon/favicon.ico' }
    ]
  },

  /*
  ** Customize the progress bar color
  */
  loading: { color: '#63efd9' },

  /*
  ** Global CSS
  */
  css: [
    //'element-ui/lib/theme-chalk/index.css',
    'vue-ionicons/ionicons.css',
    '~/layouts/variables.scss',
  ],

  /*
  ** Add element-ui in our app, see plugins/element-ui.js file
  */
  plugins: [
    '@/plugins/element-ui',
    '@/plugins/vue-i18n',
  ],

  modules: [
    '@nuxtjs/pwa',
  ],

  /*
  ** Build configuration
  */
  build: {
    /*
    * Extend webpack configuration here
    */
    extend(config, { isDev }) {
      const path = require('path');

      /**
       * Run ESLint on save
       */
      if (isDev && process.client) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }


      //config.resolve.alias["vue-ionicons"] = path.resolve(__dirname, "node_modules/vue-ionicons");

      /**
       * Pre-render app before deploy
       */
      // if (!isDev) {
      //   const _toArray = require('lodash/toArray')
      //   const path = require('path')
      //   const PrerenderSPAPlugin = require('prerender-spa-plugin')
      //   const routes = require('./routes.config.js');
      //
      //   const webpackPlugin = new PrerenderSPAPlugin({
      //     // Required - The path to the webpack-outputted app to prerender.
      //     staticDir: path.join(__dirname, 'dist'),
      //     // Required - Routes to render.
      //     routes: _toArray(routes),
      //   });
      //
      //   config.plugins.push(webpackPlugin)
      // }
    }
  },

  /**
   * https://nuxtjs.org/api/configuration-generate
   */
  // generate: {
  //   subFolders: false,
  // },
}

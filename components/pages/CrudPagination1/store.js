import c from './const'

const store = {
  namespaced: true,

  state: {
    page: 1,
    pageSize: 10,
    totalItem: 0,
  },
  getters: {},
  mutations: {

    [c.mutation.setPage]: (state, data) => state.page = data,
    [c.mutation.setPageSize]: (state, data) => state.pageSize = data,
    [c.mutation.setTotalItem]: (state, data) => state.totalItem = data,
    [c.mutation.setPaginationData]: (state, { page, pageSize, totalItem }) => {
      console.log('setPaginationData: ');
      state.page = page;
      state.pageSize = pageSize;
      state.totalItem = totalItem;
    }

  },
  actions: {

    [c.action.changePageSize]({ dispatch, commit }, { page, pageSize }) {
      dispatch(c.action.change, { page, pageSize });
    },
    [c.action.changePage]({ dispatch, commit }, { page, pageSize }) {
      dispatch(c.action.change, { page, pageSize });
    },

    /**
     * changePageSize and changePage do the same behavior but different in payload.
     this.$store.dispatch(c.gns(c.action.changePage), {page, pageSize})
     */
    [c.action.change]({ dispatch, commit }, { page, pageSize }) {
      throw new Error('action was not implemented!')
    },

  },
  modules: {},
};


export default store;

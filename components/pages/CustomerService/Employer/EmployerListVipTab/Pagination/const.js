import cParent from '../const'
import defaultConf from '~/components/pages/CrudPagination1/const'

const thisConst = {
  ...defaultConf,
  prefix: cParent.prefix + "Pagination/",
};

export default thisConst;

// import cParent from '../const'
// export default {
//   /**
//    * Module namespace path:
//    * See more here: https://vuex.vuejs.org/en/modules.html
//    * Usage:
//    * mounted: function () {
//    *   this.$store.dispatch(c.gns(c.action.changePage));
//    * },
//    */
//   prefix: cParent.prefix + "Pagination/",
//   gns(str) {
//     console.log('this.prefix + str: ', this.prefix + str);
//     return this.prefix + str;
//   },
//
//   mutation: {
//     "setPageSize" : 'setPageSize',
//     "setPage" : 'setPage',
//     "setTotalItem" : 'setTotalItem',
//     "setPaginationData" : 'setPaginationData', // Set the setPageSize, setPage, setTotalItem
//   },
//   action: {
//     "changePageSize" : 'changePageSize',
//     "changePage" : 'changePage',
//     "change" : 'change',
//   },
// }

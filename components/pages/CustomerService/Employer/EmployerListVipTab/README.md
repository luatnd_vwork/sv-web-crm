This almost are the same to EmployerListTab. So we clone it

Clone Steps:

1. Clone `EmployerListTab` into `EmployerListVipTab` 
2. Register new Tab at: 
  Employer/store.js
  Employer/const.js

3. Change prefix at:  EmployerListVipTab/const.js
4. Find and replace with in new `EmployerListVipTab` folder (match-case): 
  * `EmployerListTab` with `EmployerListVipTab`
  * `EmployerListTab` with `EmployerListVipTab`
  
5. Set your own Form UI
6. Set your own DataTableUI

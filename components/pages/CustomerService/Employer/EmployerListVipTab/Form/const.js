import cParent from '../const'

export default {
  /**
   * Module namespace path:
   * See more here: https://vuex.vuejs.org/en/modules.html
   * Usage:
   * mounted: function () {
   *   this.$store.dispatch(c.gns(c.action.changePage));
   * },
   *
   * Remember the last `/` char in `prefix`
   *
   * NOTE: When you change folder structure like: Change name, move path, you must
   *  1. change the store path, and module for easy to read the code and maintain
   *  2. re update this prefix
   */
  prefix: cParent.prefix + 'Form/',
  gns(str) {
    return this.prefix + str;
  },

  mutation: {
    "setFormUiData" : 'setFormUiData',
    "setFormData" : 'setFormData_vip',
  },
  action: {
    "fetchFormUiData": 'fetchFormUiData',
  },
}

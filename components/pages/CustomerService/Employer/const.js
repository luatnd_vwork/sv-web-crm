export default {
  /**
   * Module namespace path:
   * See more here: https://vuex.vuejs.org/en/modules.html
   * Usage:
   * mounted: function () {
   *   this.$store.dispatch(c.gns(c.action.changePage));
   * },
   */
  prefix: 'Employer/',

  /**
   * global namespace
   *
   * @param str
   * @returns {string}
   */
  gns(str) {
    return this.prefix + str;
  },

  mutation: {
    setActiveTab: 'setActiveTab',
  },
  action: {
    activeTheTab: 'activeTheTab',
  },
  getter: {
    isActivating: 'isActivating',
  },

  /**
   * List tabs in Employer screen
   * Required for automation render: pages/SalesEmployerInformation.vue
   */
  tabs: {
    EmployerListTab: {
      name: "EmployerListTab",
      label: "Danh sách NTD",
      form: "EmployerListTabForm",
      dataTable: "EmployerListTabDataTable",
      pagination: "EmployerListTabPagination",
    },
    EmployerListVipTab: {
      name: "EmployerListVipTab",
      label: "Đang VIP",
      form: "EmployerListVipTabForm",
      dataTable: "EmployerListVipTabDataTable",
      pagination: "EmployerListVipTabPagination",
    },
    // FilterCvAccountTab: {
    //   name: "FilterCvAccountTab",
    //   label: "TK lọc hồ sơ",
    //   form: "FilterCvAccountTabForm",
    //   dataTable: "FilterCvAccountTabDataTable",
    //   pagination: "FilterCvAccountTabPagination",
    // },
  },

  getActiveTabDef: function (tabName) {
    return typeof this.tabs[tabName] !== "undefined" ? this.tabs[tabName] : null;
  },

  DEFAULT_PAGE_SIZE: 15,
}

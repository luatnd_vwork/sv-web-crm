/* eslint-disable */

import axios from 'axios'
import { encoder, STATUS_CODE, fakeApiRequestAsyncAwait } from '~/utils/HttpService'
import guid from 'uuid/v4';
import c from './const'


async function fetchFilteredNtd(payload) {
  const axiosRequest = {
    method: 'GET',
    url: 'https://jsonplaceholder.typicode.com/users',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Accept': 'application/json',
    },
    //data: encoder(payload),
    params: payload,
  };

  const {
    page = 1,
    pageSize = c.DEFAULT_PAGE_SIZE,
  } = payload;

  const ntdList = [];
  for (let i = 0; i < pageSize; i++) {
    ntdList.push({
      id: guid(),
      name: 'nha tuyen dung khong duyet ' + Math.floor(Math.random() * 9999),
      email: 'nha.tuyen.dung.xx@gmail.com',
      phone: '0987654321',
      register: '23-04-2018',
      cskh: 'vytran_htkh',
      validateStatus: [
        { id: 0, label: 'Chưa xác thực' },
        { id: 1, label: 'Bình thường' },
        { id: 2, label: 'Xác thực' },
      ][Math.floor(Math.random() * 3)],
      lastLogin: '23-04-2018',
      thamgiaPhi: '23-04-2018',
      ketthucPhi: '30-04-2018',
      gpdk: '',
    });
  }

  let data = {
    success: true,
    message: "",
    data: ntdList,
    pagination: {
      total: 123456,
      page,
      pageSize,
    }
  };

  try {
    //const response___ = await axios(axiosRequest);
    axios(axiosRequest); // Run this to see the request, just for fun.
    const response = await fakeApiRequestAsyncAwait({
      //status: Math.random() > 0.4 ? 200 : 400, // Random success or failure
      status: 200,
      data: data,
    }, 300);

    if (response.status === STATUS_CODE.SUCCESS) {
      //data = response.data;
      return data;
    } else {
      return {
        success: false,
        message: 'API failed',
      };
    }
  } catch (e) {
    console.warn('Exception: ', e.message);
    return {
      success: false,
      message: e.message,
    };
  }
}


async function fetchFilteredNtdVip(payload) {
  return fetchFilteredNtd(payload);
}

export {
  fetchFilteredNtd,
  fetchFilteredNtdVip,
}

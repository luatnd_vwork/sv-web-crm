import c from './const'
import { fetchFormUiSetting } from '../apiService'

import { sales, folders, vipStatus, cities, loginTimes, sort } from './mock'

const store = {
  namespaced: true,

  state: {
    formData: {
      // search NTD: id, email, name, phone
      universalKeyword: '',
      city: '',
      vipStatus: '',
      folder: '',
      customerCareStaffOrGroup: '',
      registerDate: [],
      loginTime: '',

      sortBy: ''
    },

    /**
     * TODO: Caching this into the local storage for 1 week.
     * Because this data is normally not change
     * OR Choose to cache this on the API call
     */
    formUiSetting: {
      //universalKeyword: '',
      city: [],
      vipStatus: [],
      folder: [],
      customerCareStaffOrGroup: [],
      //registerDate: [],
      loginTime: [],
      sortBy: [],
    },

  },
  getters: {},
  mutations: {

    [c.mutation.setFormUiData]: (state, data) => state.formUiSetting = data,
    [c.mutation.setFormData]: (state, data) => state.formData = data,

  },
  actions: {

    [c.action.fetchFormUiData]({ dispatch, commit }) {
      fetchFormUiSetting()
        .then(data => {
          // console.log('fetchFormUiSetting success: ', data);

          const formUiSetting = {
            //universalKeyword: '',
            city: cities,
            vipStatus: vipStatus,
            folder: folders,
            customerCareStaffOrGroup: sales,
            //registerDate: [],
            loginTime: loginTimes,
            sortBy: sort,
          };

          commit(c.mutation.setFormUiData, formUiSetting)
        })
        .catch(err => {
          console.warn('fetchFormUiSetting error: ', err);
        });
    },

  },
  modules: {},
};


export default store;

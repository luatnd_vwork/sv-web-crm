import c from './const'
import cListTab from '../const'
import cPagination from '../Pagination/const'
import { fetchFilteredNtd } from '../../apiService' // EmployerApiService

const store = {
  namespaced: true,

  state: {
    searchResult: {
      success: true,
      message: "",
      data: [],
      // pagination: {
      //   total: 0,
      //   page: 1,
      // }
    },
  },
  getters: {},
  mutations: {

    [c.mutation.setSearchResult]: (state, data) => state.searchResult = { ...state.searchResult, ...data },

  },

  /**
   * https://vuex.vuejs.org/en/api.html
   */
  actions: {

    [c.action.search]({ dispatch, commit, rootState }, payload) {
      let filterData = {};
      let paginationData = {};


      /**
       * 1. Case submit filter form, we dont have paginate data, we need to append paginate data
       * 2. Case change pagination form, we dont have filter form data, we need to append filter form data
       */
      switch (payload.__action) {
        case "search":
          const { page, pageSize } = rootState.Employer.EmployerListTab.Pagination;
          filterData = payload;
          paginationData = { page, pageSize };
          break;
        case "paginate":
          filterData = rootState.Employer.EmployerListTab.Form.formData;
          paginationData = { page: payload.page, pageSize: payload.pageSize };
          break;
        case "init":
          filterData = payload;
          paginationData = { page: payload.page, pageSize: payload.pageSize };
          break;
        default:
          throw new Error('payload.__action is required for search! Plz add __action to payload');
      }



      /**
       * function fetchFilteredNtd({
       *    pageSize: 10, // paginationData
       *    page: 1, // paginationData
       *
       *    sort: xxx, //filterData
       *    universalKeyword: 'xxx', //filterData
       *    cskh: null, //filterData
       * })
       */
      // Show loading state
      commit(cListTab.gns(cListTab.mutation.setSearchingState), true, { root: true });
      const searchPayload = { ...filterData, ...paginationData };

      // console.log('payload: ', payload);
      console.log('searchPayload: ', searchPayload);

      fetchFilteredNtd(searchPayload)
        .then(data => {
          /**
           * When success:
           *    Set Search Result to store
           *    Hide loading state
           *    Change pagination data
           *    Update searchPayload to URL
           */
          commit(c.mutation.setSearchResult, data);
          commit(cListTab.gns(cListTab.mutation.setSearchingState), false, { root: true });

          const { page, pageSize, total: totalItem } = data.pagination;

          // console.log('data.pagination: ', data.pagination);
          // console.log('setPaginationData payload { page, pageSize, totalItem }: ', { page, pageSize, totalItem });

          commit(cPagination.gns(cPagination.mutation.setPaginationData), { page, pageSize, totalItem }, { root: true });


          /**
           * Alias search payload to the URL so that everyone can bookmark it
           */
          delete searchPayload.__action;
          if (typeof searchPayload.__tab === "undefined") {
            searchPayload.__tab = $nuxt.$route.query.__tab;
            // searchPayload.__tab = rootState.Employer.activeTab;
          }
          this.app.router.push({ query: searchPayload });
        })
        .catch(err => {
          commit(cListTab.gns(cListTab.mutation.setSearchingState), false, { root: true });
          console.warn('fetchFilteredNtd error: ', err);
        });
    },

  },
  modules: {},
};


export default store;

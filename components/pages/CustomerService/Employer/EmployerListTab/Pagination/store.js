import c from './const'
import cDataTable from '../DataTable/const'
import defaultStore from '~/components/pages/CrudPagination1/store'
import _cloneDeep from 'lodash/cloneDeep'

const store = _cloneDeep(defaultStore);

store.actions[c.action.change] = function ({ dispatch, commit }, { page, pageSize }) {
  const searchPayload = {
    __action: 'paginate',
    page,
    pageSize,
  };

  dispatch(cDataTable.gns(cDataTable.action.search), searchPayload, { root: true })
};

export default store;

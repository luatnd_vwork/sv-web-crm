import cParent from '../const'
import defaultConf from '~/components/pages/CrudPagination1/const'

const thisConst = {
  ...defaultConf,
  prefix: cParent.prefix + "Pagination/",
};

export default thisConst;

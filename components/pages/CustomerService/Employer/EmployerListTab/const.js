import cParent from '../const'

export default {
  /**
   * Module namespace path:
   * See more here: https://vuex.vuejs.org/en/modules.html
   * Usage:
   * mounted: function () {
   *   this.$store.dispatch(c.gns(c.action.changePage));
   * },
   */
  prefix: cParent.prefix + 'EmployerListTab/',

  /**
   * global namespace
   *
   * @param str
   * @returns {string}
   */
  gns(str) {
    return this.prefix + str;
  },

  mutation: {
    "setSearchingState" : 'setSearchingState',
  },
  action: {

  },
}

/**
 * This apiService is use for the whole tab, all data table, form, pagination request was put here
 * Do not break it smaller part into data table, form, pagination because it's not necessary
 */

import axios from 'axios'
import { encoder, STATUS_CODE, fakeApiRequestAsyncAwait } from '~/utils/HttpService'
import guid from 'uuid/v4';


async function fetchFormUiSetting() {
  console.log('start fetchFormUiSetting');

  const axiosRequest = {
    method: 'GET',
    url: 'https://jsonplaceholder.typicode.com/users',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Accept': 'application/json',
    },
    //data: {action: 'fetchFormUiSetting'},
    params: {__tab: 'EmployerListTab', action: 'fetchFormUiSetting'},
  };

  // const logicErrorMsg = 'Logic error';
  // const apiErrorMsg = 'API error';
  // const exceptionErrorMsg = 'Exception error';
  //
  // /**
  //  * Standardize some data, add some missing or placeholder data
  //  * @param responseBodyData
  //  * @returns {{}}
  //  */
  // const makePayload = (responseBodyData) => responseBodyData;

  let data = {};
  try {
    const response = await axios(axiosRequest);

    if (response.status === STATUS_CODE.SUCCESS) {
      data = response.data;
    }
  } catch (e) {
    // if request failed
    console.warn('Exception: ', e.message);
  }

  return data;
}


export {
  fetchFormUiSetting,
}

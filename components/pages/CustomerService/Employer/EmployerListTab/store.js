import c from './const'
import Form from './Form/store'
import DataTable from './DataTable/store'
import Pagination from './Pagination/store'

const store = {
  namespaced: true,

  state: {
    searchingState: false,
  },
  getters: {},
  mutations: {

    [c.mutation.setSearchingState]: (state, data) => state.searchingState = data,

  },
  actions: {},
  modules: {
    Form,
    DataTable,
    Pagination,
  },
};


export default store;

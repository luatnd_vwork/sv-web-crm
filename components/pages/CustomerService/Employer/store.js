import c from './const'
//import _cloneDeep from 'lodash/cloneDeep'

import cEmployerListTab from './EmployerListTab/const'
import cEmployerListTabForm from './EmployerListTab/Form/const'
import cEmployerListTabDataTable from './EmployerListTab/DataTable/const'

import cEmployerListVipTab from './EmployerListVipTab/const'
import cEmployerListVipTabForm from './EmployerListVipTab/Form/const'
import cEmployerListVipTabDataTable from './EmployerListVipTab/DataTable/const'

import EmployerListTab from './EmployerListTab/store'
import EmployerListVipTab from './EmployerListVipTab/store'


const cTabConst = {
  [c.tabs.EmployerListTab.name]: {
    tab: cEmployerListTab,
    form: cEmployerListTabForm,
    dataTable: cEmployerListTabDataTable,
  },
  [c.tabs.EmployerListVipTab.name]: {
    tab: cEmployerListVipTab,
    form: cEmployerListVipTabForm,
    dataTable: cEmployerListVipTabDataTable,
  },
};

const store = {
  namespaced: true,

  state: {
    activeTab: c.tabs.EmployerListTab.name,
  },
  getters: {
    [c.getter.isActivating]: (state) => (tab) => state.activeTab === tab,
  },
  mutations: {

    [c.mutation.setActiveTab]: (state, data) => {
      console.log('3. mutation.setActiveTab: ');
      state.activeTab = data;
    }

  },
  actions: {
    [c.action.activeTheTab]({ dispatch, commit }, tabName) {
      console.log('4. action.activeTheTab: tabName: ', tabName);

      let tabWasChanged = true;
      //const lastRecentTab = $nuxt.$store.state.Employer.activeTab;
      const lastRecentTab = $nuxt.$route.query.__tab;
      if (lastRecentTab === tabName) {
        console.warn(`State already '${tabName}', 'activeTheTab' will exclude pushing __tab to URL query.`);
        tabWasChanged = false;
      }

      //commit(c.mutation.setActiveTab, tabName); // Already do this in the computed setter (2 way binding)



      if (typeof cTabConst[tabName] === "undefined") {
        console.error(`No cTabConst[${tabName}] definition. Plz define it first.`);
        return;
      }
      const { tab: cTab, form: cForm, dataTable: cDataTable } = cTabConst[tabName];


      // 1. Init form UI data
      dispatch(cForm.gns(cForm.action.fetchFormUiData), null, { root: true });

      // 2. Update the fresh url with tab name (no form data in url)
      let formData, paginationData;
      if (tabWasChanged) {
        // Refresh new URL so that no `data` in other tab can effect this tab
        $nuxt.$router.push({ query: { __tab: tabName } });
        formData = $nuxt.$store.state.Employer[tabName].Form.formData;
        paginationData = $nuxt.$store.state.Employer[tabName].Pagination;
      } else {
        // If tab was not change, plz take the URL data into State
        // Useful for the case USER ENTER THE PAGE:
        const tmp = getStateFromQuery($nuxt.$route);
        formData = tmp.formData;
        paginationData = tmp.paginationData;
      }

      // 3. sync query string data to form state (vue auto update this data to form UI)
      commit(cForm.gns(cForm.mutation.setFormData), { ...formData }, { root: true });

      // 4. Do search: base on form data, do search, show loading and show data table
      const searchPayload = {
        __tab: tabName,
        __action: 'init',
        ...formData,
        ...paginationData,
      };
      console.log('initial searchPayload: ', searchPayload);
      dispatch(cDataTable.gns(cDataTable.action.search), searchPayload, { root: true });


      /**
       * Another instead clearly method:
       *  1. commit form data
       *  2. commit pagination data
       *  3. dispatch a refresh
       */
    },
  },
  modules: {
    EmployerListTab,
    EmployerListVipTab,
  },
};


function getStateFromQuery($route) {
  console.log('5. getStateFromQuery: ');

  /**
   * Get filter data from query string on initial
   * Update to form state
   * Then do search to load initial DataTable
   *
   * searchPayload is the same as EmployerListTab.Form.formData + EmployerListTab.Pagination
   * searchPayload is used for initial load only
   *
   * Why not set directly:  const searchPayload = this.$route.query;   ==> You will get wrong data
   * So instead, you need to assign default value through destructuring assignment:
   */
  const {
    // search NTD: id, email, name, phone
    universalKeyword = '',
    city = '',
    vipStatus = '',
    folder = '',
    customerCareStaffOrGroup = '',
    registerDate = [],
    loginTime = '',
    sortBy = '',
    page = '1',
    pageSize = c.DEFAULT_PAGE_SIZE.toString(),
  } = $route.query;

  const formData = {
    universalKeyword,
    city,
    vipStatus,
    folder,
    customerCareStaffOrGroup,
    registerDate,
    loginTime,
    sortBy,
  };

  const paginationData = {
    page: parseInt(page), // TODO: Refactor all variable name into page, not page anymore
    pageSize: parseInt(pageSize)
  };

  return {
    formData,
    paginationData,
  }
}


export default store;

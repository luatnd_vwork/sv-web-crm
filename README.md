# sv-web-erp-demo

> Sieuviet web backend(ERP) demo by Vuejs

## Build Setup

``` bash
# install dependencies
$ npm install # Or yarn install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, checkout the [Nuxt.js docs](https://github.com/nuxt/nuxt.js).


## Folder structure (convention) in this repos:

#### Folder + Component
Firstly, this repos use Nuxt folder structure.
Secondly, Nuxt structure aim to be robust so it might be suck sometime. 
You need to keep in mind that "alway try to make your components decoupling" so that you can re-use/clone your component easily & robustly.

Should you use the structure (Follow Reactjs component structure) in the following example:
```
components/pages/CustomerService/Employer/EmployerListTab/
├── DataTable
│   ├── ItemDeleteBtn
│   │   └── index.vue
│   ├── const.js
│   ├── index.vue
│   └── store.js
├── Form
│   ├── const.js
│   ├── index.vue
│   ├── mock.js
│   └── store.js
├── Pagination
│   ├── const.js
│   ├── index.vue
│   └── store.js
├── ToolBox
├── apiService.js
├── const.js
├── mock.js
└── store.js
```
As you can see:<br/>
In EmployerListTab we have some child components:<br/>
Foreach component we have:
```
├── index.vue       ==> Your SFC (Single File Component)
├── apiService.js   ==> All your API calling were here
├── const.js        ==> All your const use in component, try to name it meaningful
├── mock.js         ==> You probably need some mock data
└── store.js        ==> Your Vuex store here, combined with const.js
```

#### SFC (Single File Component)
You should use SFC for conveniently coding.
The downside of SFC is when your component was large, it might be hard to maintain.
But plz do not let your component big, let break it down to another components or sth good.
Component go big mean bad coding structure.

Some componnent standard that we pursue (sometime it might not possible to do):
+ Decoupling between components
+ Short
+ Container + Presentational seperation
+ Simple logic
+ Avoid overkill code

## The development Cheat Sheet
Some common ideas, problems

See the [README_CheatSheet.md](README_CheatSheet.md)

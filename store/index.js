import Vuex from 'vuex'
import app from './modules/app'
import Employer from '../components/pages/CustomerService/Employer/store'


// Polyfill for `window.fetch()`
require('whatwg-fetch')

const store = () => new Vuex.Store({
  namespaced: false,
  modules: {
    app,
    Employer,
  },
})

// Check for SSR
if (typeof window !== "undefined") {
  window.RootStore = store;
}

export default store


/**
 For Vuex action pattern and best practice, the pros and cons of each practice, plz see the guide:
 [README_BestPractice.md](README_BestPractice.md)
 */
